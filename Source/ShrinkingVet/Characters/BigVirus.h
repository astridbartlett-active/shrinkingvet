#pragma once

#include "CoreMinimal.h"
#include "Base/EnemyCharacterBase.h"
#include "Base/VetProjectileBase.h"

#include "BigVirus.generated.h"

UCLASS()
class SHRINKINGVET_API ABigVirus : public AEnemyCharacterBase
{
	GENERATED_BODY()

public:
	ABigVirus();

protected:
	UFUNCTION(BlueprintCallable, Category = "Vet|Virus|Aiming")
	void Attack(AActor* who);

	UFUNCTION(BlueprintImplementableEvent, Category = "Vet|Virus|Aiming")
	void OnAttack();
	
protected:
	UPROPERTY(EditAnywhere, Category = "Vet|Virus|Aiming")
	TArray<USceneComponent*> AimingDots;
	
	UPROPERTY(EditAnywhere, Category = "Vet|Virus|Attack")
	TSubclassOf<AVetProjectileBase> ProjectileType;
};
