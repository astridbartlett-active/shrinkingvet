#include "EnemyCharacterBase.h"

#include "VetPlayableCharacterBase.h"
#include "Components/BoxComponent.h"

AEnemyCharacterBase::AEnemyCharacterBase()
{
	CollisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Damage"));
	CollisionComponent->InitBoxExtent(FVector(15, 15, 15));
	CollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AEnemyCharacterBase::BeginOverlap);
	CollisionComponent->SetupAttachment(RootComponent);
}

void AEnemyCharacterBase::BeginPlay()
{
	Health = MaxHealth;
	Super::BeginPlay();
}

void AEnemyCharacterBase::BeginOverlap(UPrimitiveComponent* overlappedComp, AActor* otherActor,
	UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& sweepResult)
{
	auto player = dynamic_cast<AVetPlayableCharacterBase*>(otherActor);
	if(player)
	{
		player->Damage(ContactDamage);
	}
}

void AEnemyCharacterBase::Damage(float amount)
{
	Health -= amount;
	OnDamage(Health <= 0 ? 0 : Health / MaxHealth);
}

bool AEnemyCharacterBase::IsDead()
{
	return Health <= 0;
}

float AEnemyCharacterBase::GetHealthPercentage()
{
	if (Health <= 0) return 0;
	return Health / MaxHealth;
}
