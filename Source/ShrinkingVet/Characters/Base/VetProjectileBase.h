#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VetProjectileBase.generated.h"

class UBoxComponent;
class UProjectileMovementComponent;
UCLASS()
class SHRINKINGVET_API AVetProjectileBase : public AActor
{
	GENERATED_BODY()
	
public:	
	AVetProjectileBase();

	void Fire(const FVector& direction, float powerPercent, bool isFromPlayer);

protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Vet|Projectile")
	void OnDestroy();
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Vet|Projectile")
	void DidDamage();

	UFUNCTION()
	virtual void OnHit(UPrimitiveComponent* hitComponent, AActor* otherActor, UPrimitiveComponent* otherComponent, FVector normalImpulse, const FHitResult& hit);

	UFUNCTION()
	virtual void BeginOverlap(UPrimitiveComponent* overlappedComp, AActor* otherActor, UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& sweepResult);

	UFUNCTION(BlueprintCallable, Category = "Vet|Projectile")
	bool GetShouldKill();
	
protected:	
	UPROPERTY(EditAnywhere, Category = "Vet|Projectile")
	UBoxComponent* CollisionComponent;
	
	UPROPERTY(VisibleAnywhere, Category = "Vet|Projectile")
	UProjectileMovementComponent* ProjectileMovementComponent;

	UPROPERTY(EditAnywhere, Category = "Vet|Projectile")
	float Damage {2};

	UPROPERTY(EditAnywhere, Category = "Vet|Projectile")
	bool ShouldKill {false};

	float Power;
	bool CanPierce {false};
	bool Fired {false};
	bool IsFromPlayer {false};
};
