#include "VetProjectileBase.h"


#include "EnemyCharacterBase.h"
#include "VetPlayableCharacterBase.h"
#include "Components/BoxComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

AVetProjectileBase::AVetProjectileBase()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	CollisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision"));
	CollisionComponent->InitBoxExtent(FVector(15, 15, 15));
	CollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AVetProjectileBase::BeginOverlap);
	CollisionComponent->OnComponentHit.AddDynamic(this, &AVetProjectileBase::OnHit);
	CollisionComponent->SetupAttachment(RootComponent);

	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Movement"));
	ProjectileMovementComponent->SetUpdatedComponent(CollisionComponent);
	ProjectileMovementComponent->InitialSpeed = 1000.0f;
	ProjectileMovementComponent->MaxSpeed = 1000.0f;
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->bShouldBounce = false;
	ProjectileMovementComponent->ProjectileGravityScale = 0.f;
}

void AVetProjectileBase::Fire(const FVector& direction, float powerPercent, bool isFromPlayer)
{
	Power = powerPercent * (1 / 0.67); // 67% = full power/speed
	CanPierce = Power > 1;
	float pwrSquared = Power * Power; // Square it to make the visual differences more pronounced
	ProjectileMovementComponent->Velocity = direction * ProjectileMovementComponent->InitialSpeed * (pwrSquared * pwrSquared);
	SetActorScale3D(FVector(pwrSquared, pwrSquared, pwrSquared));
	Fired = true;
	IsFromPlayer = isFromPlayer;
}

void AVetProjectileBase::BeginPlay()
{
	Super::BeginPlay();
}

void AVetProjectileBase::OnHit(UPrimitiveComponent* hitComponent, AActor* otherActor,
	UPrimitiveComponent* otherComponent, FVector normalImpulse, const FHitResult& hit)
{
	ShouldKill = true;
}

void AVetProjectileBase::BeginOverlap(UPrimitiveComponent* overlappedComp, AActor* otherActor,
                                      UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& sweepResult)
{
	if (ShouldKill) return;
	
	auto player = dynamic_cast<AVetPlayableCharacterBase*>(otherActor);
	auto enemy = dynamic_cast<AEnemyCharacterBase*>(otherActor);
	if (player) 
	{
		if(IsFromPlayer)
		{
			return;
		}
		else
		{
			player->Damage(Damage);
			DidDamage();
			ShouldKill = true;
		}
	}
	else if(enemy)
	{
		if(!IsFromPlayer)
		{
			return;
		}
		else
		{
			enemy->Damage(Power * Damage);
			DidDamage();
			if(CanPierce)
			{
				CanPierce = false;
			}
			else
			{
				ShouldKill = true;
			}
		}
	}
	else
	{
		ShouldKill = true;
	}
}

bool AVetProjectileBase::GetShouldKill()
{
	return ShouldKill || (Fired && ProjectileMovementComponent->Velocity.IsNearlyZero());
}
