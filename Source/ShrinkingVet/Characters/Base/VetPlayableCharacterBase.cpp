#include "VetPlayableCharacterBase.h"


#include "Blueprint/UserWidget.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

AVetPlayableCharacterBase::AVetPlayableCharacterBase()
{
	// Normal
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));

	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetRelativeLocation(FVector(0, 0, 48));
	CameraBoom->TargetArmLength = 450;
	CameraBoom->bUsePawnControlRotation = true;

	Camera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	Camera->bUsePawnControlRotation = false;

	// Aiming
	AimingCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Aiming Camera"));
	AimingCameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("Aiming CameraBoom"));
	
	AimingCameraBoom->SetupAttachment(RootComponent);
	AimingCameraBoom->SetRelativeLocation(FVector(10, 0, 52));
	AimingCameraBoom->TargetArmLength = 100;
	AimingCameraBoom->bUsePawnControlRotation = true;
	
	AimingCamera->SetupAttachment(AimingCameraBoom, USpringArmComponent::SocketName);
	AimingCamera->bUsePawnControlRotation = false;

	FireLocation = CreateDefaultSubobject<USceneComponent>(TEXT("Fire Location"));
	FireLocation->SetupAttachment(RootComponent);
	
	// More Normal Stuff
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0, 500, 0);
	GetCharacterMovement()->JumpZVelocity = 1300;
	GetCharacterMovement()->AirControl = 0.4;
	GetCharacterMovement()->MaxWalkSpeed = 1500;

	GetCapsuleComponent()->InitCapsuleSize(42, 96);

	AutoPossessPlayer = EAutoReceiveInput::Player0;
}

void AVetPlayableCharacterBase::BeginPlay()
{
	GetPlayerController()->PlayerCameraManager->ViewPitchMin = MinPitch;
	GetPlayerController()->PlayerCameraManager->ViewPitchMax = MaxPitch;
	
	SetCheckpoint(GetActorLocation());
	AimingCamera->Deactivate();

	MaxWalkSpeed = GetCharacterMovement()->MaxWalkSpeed;
	JumpVelocity = GetCharacterMovement()->JumpZVelocity;

	if (MainHud && !Hud)
	{
		Hud = CreateWidget<UUserWidget>(GetPlayerController(), MainHud);
	}
	
	Super::BeginPlay();
}

void AVetPlayableCharacterBase::InitForPlay(UUserWidget* removeMenu /* = nullptr */)
{
	if (Hud && !Hud->IsInViewport())
	{
		Hud->AddToViewport();
	}

	if(removeMenu && removeMenu->IsInViewport())
	{
		removeMenu->RemoveFromViewport();
	}

	auto pc = GetPlayerController();
	pc->bShowMouseCursor = false;
	pc->SetPause(false);
	UWidgetBlueprintLibrary::SetInputMode_GameOnly(pc);

	InMenu = false;
}

float AVetPlayableCharacterBase::Damage(float amount)
{
	if (IsInvincible) return Health / MaxHealth;
	
	Health -= amount;
	if (Health <= 0) Health = 0;
	OnDamage(Health / MaxHealth);
	return Health / MaxHealth;
}

void AVetPlayableCharacterBase::InitForMenu(UUserWidget* menu)
{
	if (Hud && Hud->IsInViewport())
	{
		Hud->RemoveFromViewport();
	}

	if (menu && !menu->IsInViewport())
	{
		menu->AddToViewport();
	}

	auto pc = GetPlayerController();
	pc->bShowMouseCursor = true;
	pc->SetPause(true);
	UWidgetBlueprintLibrary::SetInputMode_GameAndUIEx(pc, menu);

	InMenu = true;
}

APlayerController* AVetPlayableCharacterBase::GetPlayerController()
{
	return static_cast<APlayerController*>(this->Controller);
}

void AVetPlayableCharacterBase::MoveForward(float amount)
{
	Move(amount, EAxis::X);
}

void AVetPlayableCharacterBase::MoveRight(float amount)
{
	Move(amount, EAxis::Y);
}

void AVetPlayableCharacterBase::LookUp(float amount)
{
	AddControllerPitchInput(amount * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AVetPlayableCharacterBase::TurnCamera(float amount)
{
	AddControllerYawInput(amount * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AVetPlayableCharacterBase::SetCheckpoint(FVector location)
{
	LastCheckpoint = location;
}

void AVetPlayableCharacterBase::ReturnToCheckpoint()
{
	SetActorLocation(LastCheckpoint);
}

void AVetPlayableCharacterBase::StartAiming()
{
	Camera->Deactivate();
	AimingCamera->Activate();

	bUseControllerRotationYaw = true;
	GetCharacterMovement()->bOrientRotationToMovement = false;
	
	GetCharacterMovement()->MaxWalkSpeed = MaxAimSpeed;
	GetCharacterMovement()->JumpZVelocity = AimJumpVelocity;

	//GetPlayerController()->PlayerCameraManager->ViewPitchMax = 0;/*
	//GetPlayerController()->PlayerCameraManager->ViewPitchMin = 0;*/

	IsAiming = true;
}

void AVetPlayableCharacterBase::StopAiming()
{
	Camera->Activate();
	AimingCamera->Deactivate();

	bUseControllerRotationYaw = false;
	GetCharacterMovement()->bOrientRotationToMovement = true;
	
	GetCharacterMovement()->MaxWalkSpeed = MaxWalkSpeed;
	GetCharacterMovement()->JumpZVelocity = JumpVelocity;

	//GetPlayerController()->PlayerCameraManager->ViewPitchMin = MinPitch;
	//GetPlayerController()->PlayerCameraManager->ViewPitchMax = MaxPitch;
	
	IsAiming = false;
	CurrentCharge = 0;
}

float AVetPlayableCharacterBase::ChargeWeapon(float deltaSeconds)
{
	if(IsAiming)
	{
		CurrentCharge += ChargeRate * deltaSeconds;
		if(CurrentCharge >= MaxCharge)
		{
			CurrentCharge = MaxCharge;
		}
	}

	return CurrentCharge / MaxCharge;
}

bool AVetPlayableCharacterBase::Fire()
{
	float power = CurrentCharge / MaxCharge;
	if(IsAiming && power >= 0.33 && ProjectileType)
	{
		auto fireLoc = FireLocation->GetRelativeLocation();
		FRotator rotation = GetActorRotation();
		FVector location = rotation.RotateVector(fireLoc) + GetActorLocation();

		FActorSpawnParameters spawnParams;
		spawnParams.Owner = this;
		spawnParams.Instigator = GetInstigator();

		auto projectile = GetWorld()->SpawnActor<AVetProjectileBase>(ProjectileType, location, rotation);
		auto direction = AimingCamera->GetComponentRotation().Vector();
		projectile->Fire(direction, power, true);

		CurrentCharge = 0;

		return true;
	}
	
	return false;
}

UUserWidget* AVetPlayableCharacterBase::GetHud()
{
	return Hud;
}

void AVetPlayableCharacterBase::SetInvincible(bool isInvincible)
{
	IsInvincible = isInvincible;
}

void AVetPlayableCharacterBase::Move(float amount, EAxis::Type axis)
{
	auto rot = Controller->GetControlRotation();
	auto yaw = FRotator(0, rot.Yaw, 0);

	auto dir = FRotationMatrix(yaw).GetUnitAxis(axis);
	AddMovementInput(dir, amount);
}
