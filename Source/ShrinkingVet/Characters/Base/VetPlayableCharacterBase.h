#pragma once

#include "CoreMinimal.h"

#include "VetProjectileBase.h"
#include "GameFramework/Character.h"
#include "VetPlayableCharacterBase.generated.h"

class UCameraComponent;
class USpringArmComponent;

UCLASS()
class SHRINKINGVET_API AVetPlayableCharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	AVetPlayableCharacterBase();
	
	UFUNCTION(BlueprintCallable, Category = "Vet|Player|Health")
	float Damage(float amount);

protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "Vet|Player|Movement")
	void MoveForward(float amount);
	
	UFUNCTION(BlueprintCallable, Category = "Vet|Player|Movement")
	void MoveRight(float amount);

	UFUNCTION(BlueprintCallable, Category = "Vet|Player|Movement")
	void LookUp(float amount);
	
	UFUNCTION(BlueprintCallable, Category = "Vet|Player|Movement")
	void TurnCamera(float amount);

	UFUNCTION(BlueprintCallable, Category = "Vet|Player|Movement")
	void SetCheckpoint(FVector location);
	
	UFUNCTION(BlueprintCallable, Category = "Vet|Player|Movement")
	void ReturnToCheckpoint();

	UFUNCTION(BlueprintCallable, Category = "Vet|Player|Movement")
	void StartAiming();

	UFUNCTION(BlueprintCallable, Category = "Vet|Player|Movement")
	void StopAiming();

	UFUNCTION(BlueprintCallable, Category = "Vet|Player|Attack")
	float ChargeWeapon(float deltaSeconds);

	UFUNCTION(BlueprintCallable, Category = "Vet|Player|Attack")
	bool Fire();

	UFUNCTION(BlueprintCallable, Category = "Vet|Player|Menu")
	UUserWidget* GetHud();

	UFUNCTION(BlueprintCallable, Category = "Vet|Player|Menu")
	void InitForMenu(UUserWidget* menu);

	UFUNCTION(BlueprintCallable, Category = "Vet|Player|Menu")
	void InitForPlay(UUserWidget* removeMenu = nullptr);

	UFUNCTION(BlueprintImplementableEvent, Category = "Vet|Player|Health")
	void OnDamage(float percentHealthLeft);
	
	UFUNCTION(BlueprintCallable, Category = "Vet|Player|Health")
	void SetInvincible(bool isInvincible);

	void Move(float amount, EAxis::Type axis);

	APlayerController* GetPlayerController();
	
protected:	
	
	UPROPERTY(EditAnywhere, Category = "Vet|Player|Camera")
	UCameraComponent* Camera;
	
	UPROPERTY(EditAnywhere, Category = "Vet|Player|Camera")
	USpringArmComponent* CameraBoom;
	
	UPROPERTY(EditAnywhere, Category = "Vet|Player|Camera")
	UCameraComponent* AimingCamera;
	
	UPROPERTY(EditAnywhere, Category = "Vet|Player|Camera")
	USpringArmComponent* AimingCameraBoom;

	UPROPERTY(EditAnywhere, Category = "Vet|Player|Camera")
	float BaseTurnRate {45};

	UPROPERTY(EditAnywhere, Category = "Vet|Player|Camera")
	float BaseLookUpRate {45};

	UPROPERTY(EditAnywhere, Category = "Vet|Player|Camera")
	float MinPitch {-55};

	UPROPERTY(EditAnywhere, Category = "Vet|Player|Camera")
	float MaxPitch{65};

	UPROPERTY(BlueprintReadOnly)
	FVector LastCheckpoint;

	UPROPERTY(EditAnywhere, Category = "Vet|Player|Movement")
	float MaxAimSpeed {700};
	
	UPROPERTY(EditAnywhere, Category = "Vet|Player|Movement")
	float AimJumpVelocity {700};
	
	UPROPERTY(BlueprintReadOnly, Category = "Vet|Player|Attack")
	bool IsAiming {false};
	
	UPROPERTY(EditAnywhere, Category = "Vet|Player|Attack")
	float MaxCharge {100};
	
	UPROPERTY(EditAnywhere, Category = "Vet|Player|Attack")
	float ChargeRate {10};
	
	UPROPERTY(BlueprintReadOnly, Category = "Vet|Player|Attack")
	float CurrentCharge {0};
	
	UPROPERTY(BlueprintReadOnly, Category = "Vet|Player|Attack")
	float MaxHealth {100};

	UPROPERTY(EditAnywhere, Category = "Vet|Player|HUD")
	TSubclassOf<UUserWidget> MainHud;

	UPROPERTY(EditAnywhere, Category = "Vet|Player|Attack")
	USceneComponent* FireLocation;

	UPROPERTY(EditAnywhere, Category = "Vet|Player|Attack")
	TSubclassOf<AVetProjectileBase> ProjectileType;
	
	UUserWidget* Hud;

	bool InMenu {false};

	bool IsInvincible {false};
	
	float MaxWalkSpeed {700};
	float JumpVelocity {700};

	float Health {100};
};
