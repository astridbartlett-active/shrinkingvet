#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EnemyCharacterBase.generated.h"

class UBoxComponent;
UCLASS()
class SHRINKINGVET_API AEnemyCharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	AEnemyCharacterBase();

protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Vet|Enemy")
	void OnDamage(float healthLeftPercent);
	
	UFUNCTION()
	virtual void BeginOverlap(UPrimitiveComponent* overlappedComp, AActor* otherActor, UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& sweepResult);

	
public:
	UFUNCTION(BlueprintCallable, Category = "Vet|Enemy")
	void Damage(float amount);

	UFUNCTION(BlueprintCallable, Category = "Vet|Enemy")
	bool IsDead();

	UFUNCTION(BlueprintCallable, Category = "Vet|Enemy")
	float GetHealthPercentage();

protected:
	
	UPROPERTY(EditAnywhere, Category = "Vet|Enemy")
	UBoxComponent* CollisionComponent;
	
	UPROPERTY(EditAnywhere, Category = "Vet|Enemy")
	float MaxHealth {5};

	UPROPERTY(BlueprintReadOnly, Category = "Vet|Enemy")
	float Health {5};

	UPROPERTY(EditAnywhere, Category = "Vet|Enemy")
	float ContactDamage {5};
};
