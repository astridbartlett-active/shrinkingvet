#include "BigVirus.h"

ABigVirus::ABigVirus()
{
	for(int i = 0; i < 8; i++)
	{
		auto dot = CreateDefaultSubobject<USceneComponent>(TEXT("Aiming Dot " + i));
		dot->SetupAttachment(RootComponent);
		dot->SetRelativeRotation(FRotator(0, (360 / 8) * i, 0));
		AimingDots.Add(dot);
	}
}

void ABigVirus::Attack(AActor* who)
{
	if(ProjectileType && !IsDead())
	{
		float power = 1.2f;
		bool atPlayer = false;
		float healthPercent = Health / MaxHealth;

		if (healthPercent < 0.75) power = 1.3f;
		if (healthPercent < 0.5) atPlayer = true;
		if (healthPercent < 0.25) power = 1.4f;
		
		for(int i = 0; i < AimingDots.Num(); i++)
		{
			auto dot = AimingDots[i];
			auto fireLoc = dot->GetRelativeLocation();
			FRotator rotation = GetActorRotation();
			FVector location = rotation.RotateVector(fireLoc) + GetActorLocation();

			if (atPlayer)
			{
				auto at = (who->GetActorLocation() - location);
				at.Normalize();
				rotation = at.Rotation();
			}
			else
			{
				rotation += dot->GetRelativeRotation();
				rotation.Pitch = FMath::RandRange(-80, 0);
			}
		
			FActorSpawnParameters spawnParams;
			spawnParams.Owner = this;
			spawnParams.Instigator = GetInstigator();

			auto projectile = GetWorld()->SpawnActor<AVetProjectileBase>(ProjectileType, location, rotation);
			auto direction = rotation.Vector();
			projectile->Fire(direction, power, false);
			OnAttack();
		}
	}
}
