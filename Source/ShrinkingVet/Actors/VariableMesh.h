#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VariableMesh.generated.h"

UCLASS()
class SHRINKINGVET_API AVariableMesh : public AActor
{
	GENERATED_BODY()
	
public:	
	AVariableMesh();

protected:
	virtual void BeginPlay() override;

public:

	UPROPERTY(EditAnywhere, Category = "Vet|VariableMesh")
	UStaticMeshComponent* BaseMesh;
	
	UPROPERTY(EditAnywhere, Category = "Vet|VariableMesh")
	TArray<UStaticMesh*> Meshes;

};
