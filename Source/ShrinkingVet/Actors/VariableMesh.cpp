#include "VariableMesh.h"

AVariableMesh::AVariableMesh()
{
	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Base Mesh"));
}

void AVariableMesh::BeginPlay()
{
	auto mesh = FMath::RandRange(0, Meshes.Num() - 1);
	BaseMesh->SetStaticMesh(Meshes[mesh]);
	Super::BeginPlay();
}
